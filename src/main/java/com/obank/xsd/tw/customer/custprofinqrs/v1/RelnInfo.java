
package com.obank.xsd.tw.customer.custprofinqrs.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>RelnInfo complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="RelnInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RelnCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RelnCustId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RevRelnCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RelnInfo", propOrder = {
    "relnCode",
    "relnCustId",
    "revRelnCode"
})
public class RelnInfo {

    @XmlElement(name = "RelnCode")
    protected String relnCode;
    @XmlElement(name = "RelnCustId")
    protected String relnCustId;
    @XmlElement(name = "RevRelnCode")
    protected String revRelnCode;

    /**
     * 取得 relnCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelnCode() {
        return relnCode;
    }

    /**
     * 設定 relnCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelnCode(String value) {
        this.relnCode = value;
    }

    /**
     * 取得 relnCustId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRelnCustId() {
        return relnCustId;
    }

    /**
     * 設定 relnCustId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRelnCustId(String value) {
        this.relnCustId = value;
    }

    /**
     * 取得 revRelnCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRevRelnCode() {
        return revRelnCode;
    }

    /**
     * 設定 revRelnCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRevRelnCode(String value) {
        this.revRelnCode = value;
    }

}
