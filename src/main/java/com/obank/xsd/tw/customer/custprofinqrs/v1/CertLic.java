
package com.obank.xsd.tw.customer.custprofinqrs.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>CertLic complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="CertLic"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CertLicId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CertLicName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CertLicHldrName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CertLicIssAuth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CertLicIssDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="CertLicExpAuth" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CertLic", propOrder = {
    "certLicId",
    "certLicName",
    "certLicHldrName",
    "certLicIssAuth",
    "certLicIssDate",
    "certLicExpAuth"
})
public class CertLic {

    @XmlElement(name = "CertLicId")
    protected String certLicId;
    @XmlElement(name = "CertLicName")
    protected String certLicName;
    @XmlElement(name = "CertLicHldrName")
    protected String certLicHldrName;
    @XmlElement(name = "CertLicIssAuth")
    protected String certLicIssAuth;
    @XmlElement(name = "CertLicIssDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar certLicIssDate;
    @XmlElement(name = "CertLicExpAuth")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar certLicExpAuth;

    /**
     * 取得 certLicId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertLicId() {
        return certLicId;
    }

    /**
     * 設定 certLicId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertLicId(String value) {
        this.certLicId = value;
    }

    /**
     * 取得 certLicName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertLicName() {
        return certLicName;
    }

    /**
     * 設定 certLicName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertLicName(String value) {
        this.certLicName = value;
    }

    /**
     * 取得 certLicHldrName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertLicHldrName() {
        return certLicHldrName;
    }

    /**
     * 設定 certLicHldrName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertLicHldrName(String value) {
        this.certLicHldrName = value;
    }

    /**
     * 取得 certLicIssAuth 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertLicIssAuth() {
        return certLicIssAuth;
    }

    /**
     * 設定 certLicIssAuth 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertLicIssAuth(String value) {
        this.certLicIssAuth = value;
    }

    /**
     * 取得 certLicIssDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCertLicIssDate() {
        return certLicIssDate;
    }

    /**
     * 設定 certLicIssDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCertLicIssDate(XMLGregorianCalendar value) {
        this.certLicIssDate = value;
    }

    /**
     * 取得 certLicExpAuth 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCertLicExpAuth() {
        return certLicExpAuth;
    }

    /**
     * 設定 certLicExpAuth 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCertLicExpAuth(XMLGregorianCalendar value) {
        this.certLicExpAuth = value;
    }

}
