
package com.obank.xsd.tw.customer.custprofinqrs.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>CtctSTat complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="CtctSTat"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LCtctSTat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LCtctType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LCtctDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CtctSTat", propOrder = {
    "lCtctSTat",
    "lCtctType",
    "lCtctDate"
})
public class CtctSTat {

    @XmlElement(name = "LCtctSTat")
    protected String lCtctSTat;
    @XmlElement(name = "LCtctType")
    protected String lCtctType;
    @XmlElement(name = "LCtctDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar lCtctDate;

    /**
     * 取得 lCtctSTat 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLCtctSTat() {
        return lCtctSTat;
    }

    /**
     * 設定 lCtctSTat 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLCtctSTat(String value) {
        this.lCtctSTat = value;
    }

    /**
     * 取得 lCtctType 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLCtctType() {
        return lCtctType;
    }

    /**
     * 設定 lCtctType 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLCtctType(String value) {
        this.lCtctType = value;
    }

    /**
     * 取得 lCtctDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLCtctDate() {
        return lCtctDate;
    }

    /**
     * 設定 lCtctDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLCtctDate(XMLGregorianCalendar value) {
        this.lCtctDate = value;
    }

}
