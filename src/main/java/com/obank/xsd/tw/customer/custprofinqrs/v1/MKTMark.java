
package com.obank.xsd.tw.customer.custprofinqrs.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>MKTMark complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="MKTMark"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LMKTMark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LMKTUpdtDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="LMKTUpdtEmplId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MKTMark", propOrder = {
    "lmktMark",
    "lmktUpdtDate",
    "lmktUpdtEmplId"
})
public class MKTMark {

    @XmlElement(name = "LMKTMark")
    protected String lmktMark;
    @XmlElement(name = "LMKTUpdtDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar lmktUpdtDate;
    @XmlElement(name = "LMKTUpdtEmplId")
    protected String lmktUpdtEmplId;

    /**
     * 取得 lmktMark 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLMKTMark() {
        return lmktMark;
    }

    /**
     * 設定 lmktMark 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLMKTMark(String value) {
        this.lmktMark = value;
    }

    /**
     * 取得 lmktUpdtDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLMKTUpdtDate() {
        return lmktUpdtDate;
    }

    /**
     * 設定 lmktUpdtDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLMKTUpdtDate(XMLGregorianCalendar value) {
        this.lmktUpdtDate = value;
    }

    /**
     * 取得 lmktUpdtEmplId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLMKTUpdtEmplId() {
        return lmktUpdtEmplId;
    }

    /**
     * 設定 lmktUpdtEmplId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLMKTUpdtEmplId(String value) {
        this.lmktUpdtEmplId = value;
    }

}
