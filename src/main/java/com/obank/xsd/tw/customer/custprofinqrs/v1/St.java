
package com.obank.xsd.tw.customer.custprofinqrs.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>St complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="St"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GBSt" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TWSt" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "St", propOrder = {
    "gbSt",
    "twSt"
})
public class St {

    @XmlElement(name = "GBSt", required = true)
    protected String gbSt;
    @XmlElement(name = "TWSt", required = true)
    protected String twSt;

    /**
     * 取得 gbSt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGBSt() {
        return gbSt;
    }

    /**
     * 設定 gbSt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGBSt(String value) {
        this.gbSt = value;
    }

    /**
     * 取得 twSt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTWSt() {
        return twSt;
    }

    /**
     * 設定 twSt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTWSt(String value) {
        this.twSt = value;
    }

}
