
package com.obank.xsd.tw.customer.custprofinqrs.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>CustProfInfo complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="CustProfInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CIFNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Mnem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CustShrtName" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="CustEngName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CustChnName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CustPinYinName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClsDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}MailAddr" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}BirthAddr" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}RelnInfo" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Sctr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AOEmplNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Indstry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Tgt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Natl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CustStat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Res" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CntctDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="MGM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}CertLic" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="OffPhnNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Lang" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AcctRstrctCode" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="LAcctRstrctCode" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Titl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="GvnName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FmlyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Gndr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BirthDay" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="MrtlStat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NoOfChild" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PhnNo1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MobNo1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EMailAddr1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}Empl" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}ResStat" minOccurs="0"/&gt;
 *         &lt;element name="Intrst" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FaxNo1" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}NameChngHist" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="CIFAddDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="EffDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="CustType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LastVrfyDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="SpknLang" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PastTimes" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FrthrDtls" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="OthrNatl" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Ovrrd" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="RecStat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CurRecNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EntryEmplNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UpdtDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AuthEmplId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BrchId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DeptId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AuthCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AuthDtTim" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LPIDNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LTaxFreeFlg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LPIPAFlg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LVIPCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LLoanGrp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LRelnCMCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LFXId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LIndstryType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LPIDIssStat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LPIDIssLoc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LPIDIssDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="LIndstryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LIndstryObj" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LNHIFlg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LReconStat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LRes183Flg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}MKTMark" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}CrMark" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="LPrsnNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}CtctSTat" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="LCtctPhnExt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LDistrt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LCampId" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="LCashRMCode" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="LLoanRMCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LWMRMCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LGrpXSellFlg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="L3PtyXSellFlg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LFATCAId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LCarDtls" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="LNCDHldr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LCustType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LPayRollCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FATCASgndDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="FATCADocName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FATCAExpDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}LSrc" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="LAMLFlg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LChanCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LCollBrchId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LInpBrchId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LTxnNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LVerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LBussType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InpBrchId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SigFlg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}AddrRtrnStatCode" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}RsdntAddr" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustProfInfo", propOrder = {
    "cifNo",
    "mnem",
    "custShrtName",
    "custEngName",
    "custChnName",
    "custPinYinName",
    "clsDate",
    "mailAddr",
    "birthAddr",
    "relnInfo",
    "sctr",
    "aoEmplNo",
    "indstry",
    "tgt",
    "natl",
    "custStat",
    "res",
    "cntctDate",
    "mgm",
    "certLic",
    "offPhnNo",
    "lang",
    "acctRstrctCode",
    "lAcctRstrctCode",
    "titl",
    "gvnName",
    "fmlyName",
    "gndr",
    "birthDay",
    "mrtlStat",
    "noOfChild",
    "phnNo1",
    "mobNo1",
    "eMailAddr1",
    "empl",
    "resStat",
    "intrst",
    "faxNo1",
    "nameChngHist",
    "cifAddDate",
    "effDate",
    "custType",
    "lastVrfyDate",
    "spknLang",
    "pastTimes",
    "frthrDtls",
    "othrNatl",
    "ovrrd",
    "recStat",
    "curRecNo",
    "entryEmplNo",
    "updtDate",
    "authEmplId",
    "brchId",
    "deptId",
    "authCode",
    "authDtTim",
    "lpidNo",
    "lTaxFreeFlg",
    "lpipaFlg",
    "lvipCode",
    "lLoanGrp",
    "lRelnCMCode",
    "lfxId",
    "lIndstryType",
    "lpidIssStat",
    "lpidIssLoc",
    "lpidIssDate",
    "lIndstryCode",
    "lIndstryObj",
    "lnhiFlg",
    "lReconStat",
    "lRes183Flg",
    "mktMark",
    "crMark",
    "lPrsnNote",
    "ctctSTat",
    "lCtctPhnExt",
    "lCity",
    "lDistrt",
    "lCampId",
    "lCashRMCode",
    "lLoanRMCode",
    "lwmrmCode",
    "lGrpXSellFlg",
    "l3PtyXSellFlg",
    "lfatcaId",
    "lCarDtls",
    "lncdHldr",
    "lCustType",
    "lPayRollCode",
    "fatcaSgndDate",
    "fatcaDocName",
    "fatcaExpDate",
    "lSrc",
    "lamlFlg",
    "lChanCode",
    "lCollBrchId",
    "lInpBrchId",
    "lTxnNo",
    "lVerName",
    "lBussType",
    "inpBrchId",
    "sigFlg",
    "addrRtrnStatCode",
    "rsdntAddr"
})
public class CustProfInfo {

    @XmlElement(name = "CIFNo", required = true)
    protected String cifNo;
    @XmlElement(name = "Mnem")
    protected String mnem;
    @XmlElement(name = "CustShrtName")
    protected List<String> custShrtName;
    @XmlElement(name = "CustEngName")
    protected String custEngName;
    @XmlElement(name = "CustChnName")
    protected String custChnName;
    @XmlElement(name = "CustPinYinName")
    protected String custPinYinName;
    @XmlElement(name = "ClsDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar clsDate;
    @XmlElement(name = "MailAddr")
    protected MailAddr mailAddr;
    @XmlElement(name = "BirthAddr")
    protected BirthAddr birthAddr;
    @XmlElement(name = "RelnInfo")
    protected List<RelnInfo> relnInfo;
    @XmlElement(name = "Sctr")
    protected String sctr;
    @XmlElement(name = "AOEmplNo")
    protected String aoEmplNo;
    @XmlElement(name = "Indstry")
    protected String indstry;
    @XmlElement(name = "Tgt")
    protected String tgt;
    @XmlElement(name = "Natl")
    protected String natl;
    @XmlElement(name = "CustStat")
    protected String custStat;
    @XmlElement(name = "Res")
    protected String res;
    @XmlElement(name = "CntctDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar cntctDate;
    @XmlElement(name = "MGM")
    protected String mgm;
    @XmlElement(name = "CertLic")
    protected List<CertLic> certLic;
    @XmlElement(name = "OffPhnNo")
    protected String offPhnNo;
    @XmlElement(name = "Lang")
    protected String lang;
    @XmlElement(name = "AcctRstrctCode")
    protected List<String> acctRstrctCode;
    @XmlElement(name = "LAcctRstrctCode")
    protected List<String> lAcctRstrctCode;
    @XmlElement(name = "Titl")
    protected String titl;
    @XmlElement(name = "GvnName")
    protected String gvnName;
    @XmlElement(name = "FmlyName")
    protected String fmlyName;
    @XmlElement(name = "Gndr")
    protected String gndr;
    @XmlElement(name = "BirthDay")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar birthDay;
    @XmlElement(name = "MrtlStat")
    protected String mrtlStat;
    @XmlElement(name = "NoOfChild")
    protected String noOfChild;
    @XmlElement(name = "PhnNo1")
    protected String phnNo1;
    @XmlElement(name = "MobNo1")
    protected String mobNo1;
    @XmlElement(name = "EMailAddr1")
    protected String eMailAddr1;
    @XmlElement(name = "Empl")
    protected List<Empl> empl;
    @XmlElement(name = "ResStat")
    protected ResStat resStat;
    @XmlElement(name = "Intrst")
    protected List<String> intrst;
    @XmlElement(name = "FaxNo1")
    protected List<String> faxNo1;
    @XmlElement(name = "NameChngHist")
    protected List<NameChngHist> nameChngHist;
    @XmlElement(name = "CIFAddDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar cifAddDate;
    @XmlElement(name = "EffDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar effDate;
    @XmlElement(name = "CustType")
    protected String custType;
    @XmlElement(name = "LastVrfyDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar lastVrfyDate;
    @XmlElement(name = "SpknLang")
    protected List<String> spknLang;
    @XmlElement(name = "PastTimes")
    protected List<String> pastTimes;
    @XmlElement(name = "FrthrDtls")
    protected List<String> frthrDtls;
    @XmlElement(name = "OthrNatl")
    protected List<String> othrNatl;
    @XmlElement(name = "Ovrrd")
    protected List<String> ovrrd;
    @XmlElement(name = "RecStat")
    protected String recStat;
    @XmlElement(name = "CurRecNo")
    protected String curRecNo;
    @XmlElement(name = "EntryEmplNo")
    protected String entryEmplNo;
    @XmlElement(name = "UpdtDate")
    protected String updtDate;
    @XmlElement(name = "AuthEmplId")
    protected String authEmplId;
    @XmlElement(name = "BrchId")
    protected String brchId;
    @XmlElement(name = "DeptId")
    protected String deptId;
    @XmlElement(name = "AuthCode")
    protected String authCode;
    @XmlElement(name = "AuthDtTim")
    protected String authDtTim;
    @XmlElement(name = "LPIDNo")
    protected String lpidNo;
    @XmlElement(name = "LTaxFreeFlg")
    protected String lTaxFreeFlg;
    @XmlElement(name = "LPIPAFlg")
    protected String lpipaFlg;
    @XmlElement(name = "LVIPCode")
    protected String lvipCode;
    @XmlElement(name = "LLoanGrp")
    protected String lLoanGrp;
    @XmlElement(name = "LRelnCMCode")
    protected String lRelnCMCode;
    @XmlElement(name = "LFXId")
    protected String lfxId;
    @XmlElement(name = "LIndstryType")
    protected String lIndstryType;
    @XmlElement(name = "LPIDIssStat")
    protected String lpidIssStat;
    @XmlElement(name = "LPIDIssLoc")
    protected String lpidIssLoc;
    @XmlElement(name = "LPIDIssDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar lpidIssDate;
    @XmlElement(name = "LIndstryCode")
    protected String lIndstryCode;
    @XmlElement(name = "LIndstryObj")
    protected String lIndstryObj;
    @XmlElement(name = "LNHIFlg")
    protected String lnhiFlg;
    @XmlElement(name = "LReconStat")
    protected String lReconStat;
    @XmlElement(name = "LRes183Flg")
    protected String lRes183Flg;
    @XmlElement(name = "MKTMark")
    protected List<MKTMark> mktMark;
    @XmlElement(name = "CrMark")
    protected List<CrMark> crMark;
    @XmlElement(name = "LPrsnNote")
    protected String lPrsnNote;
    @XmlElement(name = "CtctSTat")
    protected List<CtctSTat> ctctSTat;
    @XmlElement(name = "LCtctPhnExt")
    protected String lCtctPhnExt;
    @XmlElement(name = "LCity")
    protected String lCity;
    @XmlElement(name = "LDistrt")
    protected String lDistrt;
    @XmlElement(name = "LCampId")
    protected List<String> lCampId;
    @XmlElement(name = "LCashRMCode")
    protected List<String> lCashRMCode;
    @XmlElement(name = "LLoanRMCode")
    protected String lLoanRMCode;
    @XmlElement(name = "LWMRMCode")
    protected String lwmrmCode;
    @XmlElement(name = "LGrpXSellFlg")
    protected String lGrpXSellFlg;
    @XmlElement(name = "L3PtyXSellFlg")
    protected String l3PtyXSellFlg;
    @XmlElement(name = "LFATCAId")
    protected String lfatcaId;
    @XmlElement(name = "LCarDtls")
    protected List<String> lCarDtls;
    @XmlElement(name = "LNCDHldr")
    protected String lncdHldr;
    @XmlElement(name = "LCustType")
    protected String lCustType;
    @XmlElement(name = "LPayRollCode")
    protected String lPayRollCode;
    @XmlElement(name = "FATCASgndDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fatcaSgndDate;
    @XmlElement(name = "FATCADocName")
    protected String fatcaDocName;
    @XmlElement(name = "FATCAExpDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fatcaExpDate;
    @XmlElement(name = "LSrc")
    protected List<LSrc> lSrc;
    @XmlElement(name = "LAMLFlg")
    protected String lamlFlg;
    @XmlElement(name = "LChanCode")
    protected String lChanCode;
    @XmlElement(name = "LCollBrchId")
    protected String lCollBrchId;
    @XmlElement(name = "LInpBrchId")
    protected String lInpBrchId;
    @XmlElement(name = "LTxnNo")
    protected String lTxnNo;
    @XmlElement(name = "LVerName")
    protected String lVerName;
    @XmlElement(name = "LBussType")
    protected String lBussType;
    @XmlElement(name = "InpBrchId")
    protected String inpBrchId;
    @XmlElement(name = "SigFlg")
    protected String sigFlg;
    @XmlElement(name = "AddrRtrnStatCode")
    protected List<AddrRtrnStatCode> addrRtrnStatCode;
    @XmlElement(name = "RsdntAddr")
    protected RsdntAddr rsdntAddr;

    /**
     * 取得 cifNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCIFNo() {
        return cifNo;
    }

    /**
     * 設定 cifNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCIFNo(String value) {
        this.cifNo = value;
    }

    /**
     * 取得 mnem 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMnem() {
        return mnem;
    }

    /**
     * 設定 mnem 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMnem(String value) {
        this.mnem = value;
    }

    /**
     * Gets the value of the custShrtName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the custShrtName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustShrtName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCustShrtName() {
        if (custShrtName == null) {
            custShrtName = new ArrayList<String>();
        }
        return this.custShrtName;
    }

    /**
     * 取得 custEngName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustEngName() {
        return custEngName;
    }

    /**
     * 設定 custEngName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustEngName(String value) {
        this.custEngName = value;
    }

    /**
     * 取得 custChnName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustChnName() {
        return custChnName;
    }

    /**
     * 設定 custChnName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustChnName(String value) {
        this.custChnName = value;
    }

    /**
     * 取得 custPinYinName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustPinYinName() {
        return custPinYinName;
    }

    /**
     * 設定 custPinYinName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustPinYinName(String value) {
        this.custPinYinName = value;
    }

    /**
     * 取得 clsDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getClsDate() {
        return clsDate;
    }

    /**
     * 設定 clsDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setClsDate(XMLGregorianCalendar value) {
        this.clsDate = value;
    }

    /**
     * 取得 mailAddr 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link MailAddr }
     *     
     */
    public MailAddr getMailAddr() {
        return mailAddr;
    }

    /**
     * 設定 mailAddr 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link MailAddr }
     *     
     */
    public void setMailAddr(MailAddr value) {
        this.mailAddr = value;
    }

    /**
     * 取得 birthAddr 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link BirthAddr }
     *     
     */
    public BirthAddr getBirthAddr() {
        return birthAddr;
    }

    /**
     * 設定 birthAddr 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link BirthAddr }
     *     
     */
    public void setBirthAddr(BirthAddr value) {
        this.birthAddr = value;
    }

    /**
     * Gets the value of the relnInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relnInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelnInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RelnInfo }
     * 
     * 
     */
    public List<RelnInfo> getRelnInfo() {
        if (relnInfo == null) {
            relnInfo = new ArrayList<RelnInfo>();
        }
        return this.relnInfo;
    }

    /**
     * 取得 sctr 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSctr() {
        return sctr;
    }

    /**
     * 設定 sctr 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSctr(String value) {
        this.sctr = value;
    }

    /**
     * 取得 aoEmplNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAOEmplNo() {
        return aoEmplNo;
    }

    /**
     * 設定 aoEmplNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAOEmplNo(String value) {
        this.aoEmplNo = value;
    }

    /**
     * 取得 indstry 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndstry() {
        return indstry;
    }

    /**
     * 設定 indstry 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndstry(String value) {
        this.indstry = value;
    }

    /**
     * 取得 tgt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTgt() {
        return tgt;
    }

    /**
     * 設定 tgt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTgt(String value) {
        this.tgt = value;
    }

    /**
     * 取得 natl 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNatl() {
        return natl;
    }

    /**
     * 設定 natl 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNatl(String value) {
        this.natl = value;
    }

    /**
     * 取得 custStat 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustStat() {
        return custStat;
    }

    /**
     * 設定 custStat 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustStat(String value) {
        this.custStat = value;
    }

    /**
     * 取得 res 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRes() {
        return res;
    }

    /**
     * 設定 res 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRes(String value) {
        this.res = value;
    }

    /**
     * 取得 cntctDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCntctDate() {
        return cntctDate;
    }

    /**
     * 設定 cntctDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCntctDate(XMLGregorianCalendar value) {
        this.cntctDate = value;
    }

    /**
     * 取得 mgm 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMGM() {
        return mgm;
    }

    /**
     * 設定 mgm 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMGM(String value) {
        this.mgm = value;
    }

    /**
     * Gets the value of the certLic property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the certLic property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCertLic().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CertLic }
     * 
     * 
     */
    public List<CertLic> getCertLic() {
        if (certLic == null) {
            certLic = new ArrayList<CertLic>();
        }
        return this.certLic;
    }

    /**
     * 取得 offPhnNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffPhnNo() {
        return offPhnNo;
    }

    /**
     * 設定 offPhnNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffPhnNo(String value) {
        this.offPhnNo = value;
    }

    /**
     * 取得 lang 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLang() {
        return lang;
    }

    /**
     * 設定 lang 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLang(String value) {
        this.lang = value;
    }

    /**
     * Gets the value of the acctRstrctCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the acctRstrctCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcctRstrctCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getAcctRstrctCode() {
        if (acctRstrctCode == null) {
            acctRstrctCode = new ArrayList<String>();
        }
        return this.acctRstrctCode;
    }

    /**
     * Gets the value of the lAcctRstrctCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lAcctRstrctCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLAcctRstrctCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getLAcctRstrctCode() {
        if (lAcctRstrctCode == null) {
            lAcctRstrctCode = new ArrayList<String>();
        }
        return this.lAcctRstrctCode;
    }

    /**
     * 取得 titl 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitl() {
        return titl;
    }

    /**
     * 設定 titl 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitl(String value) {
        this.titl = value;
    }

    /**
     * 取得 gvnName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGvnName() {
        return gvnName;
    }

    /**
     * 設定 gvnName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGvnName(String value) {
        this.gvnName = value;
    }

    /**
     * 取得 fmlyName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFmlyName() {
        return fmlyName;
    }

    /**
     * 設定 fmlyName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFmlyName(String value) {
        this.fmlyName = value;
    }

    /**
     * 取得 gndr 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGndr() {
        return gndr;
    }

    /**
     * 設定 gndr 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGndr(String value) {
        this.gndr = value;
    }

    /**
     * 取得 birthDay 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBirthDay() {
        return birthDay;
    }

    /**
     * 設定 birthDay 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBirthDay(XMLGregorianCalendar value) {
        this.birthDay = value;
    }

    /**
     * 取得 mrtlStat 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMrtlStat() {
        return mrtlStat;
    }

    /**
     * 設定 mrtlStat 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMrtlStat(String value) {
        this.mrtlStat = value;
    }

    /**
     * 取得 noOfChild 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoOfChild() {
        return noOfChild;
    }

    /**
     * 設定 noOfChild 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoOfChild(String value) {
        this.noOfChild = value;
    }

    /**
     * 取得 phnNo1 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhnNo1() {
        return phnNo1;
    }

    /**
     * 設定 phnNo1 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhnNo1(String value) {
        this.phnNo1 = value;
    }

    /**
     * 取得 mobNo1 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobNo1() {
        return mobNo1;
    }

    /**
     * 設定 mobNo1 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobNo1(String value) {
        this.mobNo1 = value;
    }

    /**
     * 取得 eMailAddr1 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMailAddr1() {
        return eMailAddr1;
    }

    /**
     * 設定 eMailAddr1 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMailAddr1(String value) {
        this.eMailAddr1 = value;
    }

    /**
     * Gets the value of the empl property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the empl property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmpl().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Empl }
     * 
     * 
     */
    public List<Empl> getEmpl() {
        if (empl == null) {
            empl = new ArrayList<Empl>();
        }
        return this.empl;
    }

    /**
     * 取得 resStat 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link ResStat }
     *     
     */
    public ResStat getResStat() {
        return resStat;
    }

    /**
     * 設定 resStat 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link ResStat }
     *     
     */
    public void setResStat(ResStat value) {
        this.resStat = value;
    }

    /**
     * Gets the value of the intrst property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the intrst property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntrst().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIntrst() {
        if (intrst == null) {
            intrst = new ArrayList<String>();
        }
        return this.intrst;
    }

    /**
     * Gets the value of the faxNo1 property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the faxNo1 property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFaxNo1().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getFaxNo1() {
        if (faxNo1 == null) {
            faxNo1 = new ArrayList<String>();
        }
        return this.faxNo1;
    }

    /**
     * Gets the value of the nameChngHist property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the nameChngHist property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNameChngHist().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NameChngHist }
     * 
     * 
     */
    public List<NameChngHist> getNameChngHist() {
        if (nameChngHist == null) {
            nameChngHist = new ArrayList<NameChngHist>();
        }
        return this.nameChngHist;
    }

    /**
     * 取得 cifAddDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCIFAddDate() {
        return cifAddDate;
    }

    /**
     * 設定 cifAddDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCIFAddDate(XMLGregorianCalendar value) {
        this.cifAddDate = value;
    }

    /**
     * 取得 effDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffDate() {
        return effDate;
    }

    /**
     * 設定 effDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffDate(XMLGregorianCalendar value) {
        this.effDate = value;
    }

    /**
     * 取得 custType 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustType() {
        return custType;
    }

    /**
     * 設定 custType 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustType(String value) {
        this.custType = value;
    }

    /**
     * 取得 lastVrfyDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastVrfyDate() {
        return lastVrfyDate;
    }

    /**
     * 設定 lastVrfyDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastVrfyDate(XMLGregorianCalendar value) {
        this.lastVrfyDate = value;
    }

    /**
     * Gets the value of the spknLang property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the spknLang property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpknLang().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getSpknLang() {
        if (spknLang == null) {
            spknLang = new ArrayList<String>();
        }
        return this.spknLang;
    }

    /**
     * Gets the value of the pastTimes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pastTimes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPastTimes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getPastTimes() {
        if (pastTimes == null) {
            pastTimes = new ArrayList<String>();
        }
        return this.pastTimes;
    }

    /**
     * Gets the value of the frthrDtls property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the frthrDtls property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFrthrDtls().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getFrthrDtls() {
        if (frthrDtls == null) {
            frthrDtls = new ArrayList<String>();
        }
        return this.frthrDtls;
    }

    /**
     * Gets the value of the othrNatl property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the othrNatl property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOthrNatl().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getOthrNatl() {
        if (othrNatl == null) {
            othrNatl = new ArrayList<String>();
        }
        return this.othrNatl;
    }

    /**
     * Gets the value of the ovrrd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ovrrd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOvrrd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getOvrrd() {
        if (ovrrd == null) {
            ovrrd = new ArrayList<String>();
        }
        return this.ovrrd;
    }

    /**
     * 取得 recStat 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecStat() {
        return recStat;
    }

    /**
     * 設定 recStat 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecStat(String value) {
        this.recStat = value;
    }

    /**
     * 取得 curRecNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurRecNo() {
        return curRecNo;
    }

    /**
     * 設定 curRecNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurRecNo(String value) {
        this.curRecNo = value;
    }

    /**
     * 取得 entryEmplNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntryEmplNo() {
        return entryEmplNo;
    }

    /**
     * 設定 entryEmplNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntryEmplNo(String value) {
        this.entryEmplNo = value;
    }

    /**
     * 取得 updtDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdtDate() {
        return updtDate;
    }

    /**
     * 設定 updtDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdtDate(String value) {
        this.updtDate = value;
    }

    /**
     * 取得 authEmplId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthEmplId() {
        return authEmplId;
    }

    /**
     * 設定 authEmplId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthEmplId(String value) {
        this.authEmplId = value;
    }

    /**
     * 取得 brchId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrchId() {
        return brchId;
    }

    /**
     * 設定 brchId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrchId(String value) {
        this.brchId = value;
    }

    /**
     * 取得 deptId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeptId() {
        return deptId;
    }

    /**
     * 設定 deptId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeptId(String value) {
        this.deptId = value;
    }

    /**
     * 取得 authCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthCode() {
        return authCode;
    }

    /**
     * 設定 authCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthCode(String value) {
        this.authCode = value;
    }

    /**
     * 取得 authDtTim 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthDtTim() {
        return authDtTim;
    }

    /**
     * 設定 authDtTim 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthDtTim(String value) {
        this.authDtTim = value;
    }

    /**
     * 取得 lpidNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLPIDNo() {
        return lpidNo;
    }

    /**
     * 設定 lpidNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLPIDNo(String value) {
        this.lpidNo = value;
    }

    /**
     * 取得 lTaxFreeFlg 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLTaxFreeFlg() {
        return lTaxFreeFlg;
    }

    /**
     * 設定 lTaxFreeFlg 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLTaxFreeFlg(String value) {
        this.lTaxFreeFlg = value;
    }

    /**
     * 取得 lpipaFlg 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLPIPAFlg() {
        return lpipaFlg;
    }

    /**
     * 設定 lpipaFlg 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLPIPAFlg(String value) {
        this.lpipaFlg = value;
    }

    /**
     * 取得 lvipCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLVIPCode() {
        return lvipCode;
    }

    /**
     * 設定 lvipCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLVIPCode(String value) {
        this.lvipCode = value;
    }

    /**
     * 取得 lLoanGrp 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLLoanGrp() {
        return lLoanGrp;
    }

    /**
     * 設定 lLoanGrp 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLLoanGrp(String value) {
        this.lLoanGrp = value;
    }

    /**
     * 取得 lRelnCMCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLRelnCMCode() {
        return lRelnCMCode;
    }

    /**
     * 設定 lRelnCMCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLRelnCMCode(String value) {
        this.lRelnCMCode = value;
    }

    /**
     * 取得 lfxId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLFXId() {
        return lfxId;
    }

    /**
     * 設定 lfxId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLFXId(String value) {
        this.lfxId = value;
    }

    /**
     * 取得 lIndstryType 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLIndstryType() {
        return lIndstryType;
    }

    /**
     * 設定 lIndstryType 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLIndstryType(String value) {
        this.lIndstryType = value;
    }

    /**
     * 取得 lpidIssStat 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLPIDIssStat() {
        return lpidIssStat;
    }

    /**
     * 設定 lpidIssStat 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLPIDIssStat(String value) {
        this.lpidIssStat = value;
    }

    /**
     * 取得 lpidIssLoc 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLPIDIssLoc() {
        return lpidIssLoc;
    }

    /**
     * 設定 lpidIssLoc 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLPIDIssLoc(String value) {
        this.lpidIssLoc = value;
    }

    /**
     * 取得 lpidIssDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLPIDIssDate() {
        return lpidIssDate;
    }

    /**
     * 設定 lpidIssDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLPIDIssDate(XMLGregorianCalendar value) {
        this.lpidIssDate = value;
    }

    /**
     * 取得 lIndstryCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLIndstryCode() {
        return lIndstryCode;
    }

    /**
     * 設定 lIndstryCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLIndstryCode(String value) {
        this.lIndstryCode = value;
    }

    /**
     * 取得 lIndstryObj 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLIndstryObj() {
        return lIndstryObj;
    }

    /**
     * 設定 lIndstryObj 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLIndstryObj(String value) {
        this.lIndstryObj = value;
    }

    /**
     * 取得 lnhiFlg 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLNHIFlg() {
        return lnhiFlg;
    }

    /**
     * 設定 lnhiFlg 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLNHIFlg(String value) {
        this.lnhiFlg = value;
    }

    /**
     * 取得 lReconStat 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLReconStat() {
        return lReconStat;
    }

    /**
     * 設定 lReconStat 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLReconStat(String value) {
        this.lReconStat = value;
    }

    /**
     * 取得 lRes183Flg 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLRes183Flg() {
        return lRes183Flg;
    }

    /**
     * 設定 lRes183Flg 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLRes183Flg(String value) {
        this.lRes183Flg = value;
    }

    /**
     * Gets the value of the mktMark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mktMark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMKTMark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MKTMark }
     * 
     * 
     */
    public List<MKTMark> getMKTMark() {
        if (mktMark == null) {
            mktMark = new ArrayList<MKTMark>();
        }
        return this.mktMark;
    }

    /**
     * Gets the value of the crMark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the crMark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCrMark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CrMark }
     * 
     * 
     */
    public List<CrMark> getCrMark() {
        if (crMark == null) {
            crMark = new ArrayList<CrMark>();
        }
        return this.crMark;
    }

    /**
     * 取得 lPrsnNote 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLPrsnNote() {
        return lPrsnNote;
    }

    /**
     * 設定 lPrsnNote 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLPrsnNote(String value) {
        this.lPrsnNote = value;
    }

    /**
     * Gets the value of the ctctSTat property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ctctSTat property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCtctSTat().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CtctSTat }
     * 
     * 
     */
    public List<CtctSTat> getCtctSTat() {
        if (ctctSTat == null) {
            ctctSTat = new ArrayList<CtctSTat>();
        }
        return this.ctctSTat;
    }

    /**
     * 取得 lCtctPhnExt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLCtctPhnExt() {
        return lCtctPhnExt;
    }

    /**
     * 設定 lCtctPhnExt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLCtctPhnExt(String value) {
        this.lCtctPhnExt = value;
    }

    /**
     * 取得 lCity 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLCity() {
        return lCity;
    }

    /**
     * 設定 lCity 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLCity(String value) {
        this.lCity = value;
    }

    /**
     * 取得 lDistrt 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLDistrt() {
        return lDistrt;
    }

    /**
     * 設定 lDistrt 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLDistrt(String value) {
        this.lDistrt = value;
    }

    /**
     * Gets the value of the lCampId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lCampId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLCampId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getLCampId() {
        if (lCampId == null) {
            lCampId = new ArrayList<String>();
        }
        return this.lCampId;
    }

    /**
     * Gets the value of the lCashRMCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lCashRMCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLCashRMCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getLCashRMCode() {
        if (lCashRMCode == null) {
            lCashRMCode = new ArrayList<String>();
        }
        return this.lCashRMCode;
    }

    /**
     * 取得 lLoanRMCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLLoanRMCode() {
        return lLoanRMCode;
    }

    /**
     * 設定 lLoanRMCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLLoanRMCode(String value) {
        this.lLoanRMCode = value;
    }

    /**
     * 取得 lwmrmCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLWMRMCode() {
        return lwmrmCode;
    }

    /**
     * 設定 lwmrmCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLWMRMCode(String value) {
        this.lwmrmCode = value;
    }

    /**
     * 取得 lGrpXSellFlg 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLGrpXSellFlg() {
        return lGrpXSellFlg;
    }

    /**
     * 設定 lGrpXSellFlg 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLGrpXSellFlg(String value) {
        this.lGrpXSellFlg = value;
    }

    /**
     * 取得 l3PtyXSellFlg 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getL3PtyXSellFlg() {
        return l3PtyXSellFlg;
    }

    /**
     * 設定 l3PtyXSellFlg 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setL3PtyXSellFlg(String value) {
        this.l3PtyXSellFlg = value;
    }

    /**
     * 取得 lfatcaId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLFATCAId() {
        return lfatcaId;
    }

    /**
     * 設定 lfatcaId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLFATCAId(String value) {
        this.lfatcaId = value;
    }

    /**
     * Gets the value of the lCarDtls property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lCarDtls property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLCarDtls().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getLCarDtls() {
        if (lCarDtls == null) {
            lCarDtls = new ArrayList<String>();
        }
        return this.lCarDtls;
    }

    /**
     * 取得 lncdHldr 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLNCDHldr() {
        return lncdHldr;
    }

    /**
     * 設定 lncdHldr 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLNCDHldr(String value) {
        this.lncdHldr = value;
    }

    /**
     * 取得 lCustType 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLCustType() {
        return lCustType;
    }

    /**
     * 設定 lCustType 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLCustType(String value) {
        this.lCustType = value;
    }

    /**
     * 取得 lPayRollCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLPayRollCode() {
        return lPayRollCode;
    }

    /**
     * 設定 lPayRollCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLPayRollCode(String value) {
        this.lPayRollCode = value;
    }

    /**
     * 取得 fatcaSgndDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFATCASgndDate() {
        return fatcaSgndDate;
    }

    /**
     * 設定 fatcaSgndDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFATCASgndDate(XMLGregorianCalendar value) {
        this.fatcaSgndDate = value;
    }

    /**
     * 取得 fatcaDocName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFATCADocName() {
        return fatcaDocName;
    }

    /**
     * 設定 fatcaDocName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFATCADocName(String value) {
        this.fatcaDocName = value;
    }

    /**
     * 取得 fatcaExpDate 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFATCAExpDate() {
        return fatcaExpDate;
    }

    /**
     * 設定 fatcaExpDate 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFATCAExpDate(XMLGregorianCalendar value) {
        this.fatcaExpDate = value;
    }

    /**
     * Gets the value of the lSrc property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lSrc property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLSrc().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LSrc }
     * 
     * 
     */
    public List<LSrc> getLSrc() {
        if (lSrc == null) {
            lSrc = new ArrayList<LSrc>();
        }
        return this.lSrc;
    }

    /**
     * 取得 lamlFlg 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLAMLFlg() {
        return lamlFlg;
    }

    /**
     * 設定 lamlFlg 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLAMLFlg(String value) {
        this.lamlFlg = value;
    }

    /**
     * 取得 lChanCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLChanCode() {
        return lChanCode;
    }

    /**
     * 設定 lChanCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLChanCode(String value) {
        this.lChanCode = value;
    }

    /**
     * 取得 lCollBrchId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLCollBrchId() {
        return lCollBrchId;
    }

    /**
     * 設定 lCollBrchId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLCollBrchId(String value) {
        this.lCollBrchId = value;
    }

    /**
     * 取得 lInpBrchId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLInpBrchId() {
        return lInpBrchId;
    }

    /**
     * 設定 lInpBrchId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLInpBrchId(String value) {
        this.lInpBrchId = value;
    }

    /**
     * 取得 lTxnNo 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLTxnNo() {
        return lTxnNo;
    }

    /**
     * 設定 lTxnNo 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLTxnNo(String value) {
        this.lTxnNo = value;
    }

    /**
     * 取得 lVerName 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLVerName() {
        return lVerName;
    }

    /**
     * 設定 lVerName 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLVerName(String value) {
        this.lVerName = value;
    }

    /**
     * 取得 lBussType 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLBussType() {
        return lBussType;
    }

    /**
     * 設定 lBussType 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLBussType(String value) {
        this.lBussType = value;
    }

    /**
     * 取得 inpBrchId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInpBrchId() {
        return inpBrchId;
    }

    /**
     * 設定 inpBrchId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInpBrchId(String value) {
        this.inpBrchId = value;
    }

    /**
     * 取得 sigFlg 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSigFlg() {
        return sigFlg;
    }

    /**
     * 設定 sigFlg 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSigFlg(String value) {
        this.sigFlg = value;
    }

    /**
     * Gets the value of the addrRtrnStatCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addrRtrnStatCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddrRtrnStatCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AddrRtrnStatCode }
     * 
     * 
     */
    public List<AddrRtrnStatCode> getAddrRtrnStatCode() {
        if (addrRtrnStatCode == null) {
            addrRtrnStatCode = new ArrayList<AddrRtrnStatCode>();
        }
        return this.addrRtrnStatCode;
    }

    /**
     * 取得 rsdntAddr 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link RsdntAddr }
     *     
     */
    public RsdntAddr getRsdntAddr() {
        return rsdntAddr;
    }

    /**
     * 設定 rsdntAddr 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link RsdntAddr }
     *     
     */
    public void setRsdntAddr(RsdntAddr value) {
        this.rsdntAddr = value;
    }

}
