
package com.obank.xsd.tw.customer.custprofinqrs.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>LSrc complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="LSrc"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LSrcCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LSrcId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LSrc", propOrder = {
    "lSrcCode",
    "lSrcId"
})
public class LSrc {

    @XmlElement(name = "LSrcCode")
    protected String lSrcCode;
    @XmlElement(name = "LSrcId")
    protected String lSrcId;

    /**
     * 取得 lSrcCode 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLSrcCode() {
        return lSrcCode;
    }

    /**
     * 設定 lSrcCode 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLSrcCode(String value) {
        this.lSrcCode = value;
    }

    /**
     * 取得 lSrcId 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLSrcId() {
        return lSrcId;
    }

    /**
     * 設定 lSrcId 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLSrcId(String value) {
        this.lSrcId = value;
    }

}
