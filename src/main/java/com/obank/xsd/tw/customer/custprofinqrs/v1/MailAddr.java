
package com.obank.xsd.tw.customer.custprofinqrs.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>MailAddr complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="MailAddr"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}St" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}TownCnty" minOccurs="0"/&gt;
 *         &lt;element ref="{http://www.obank.com/xsd/TW/Customer/CustProfInqRs/v1}AddrZip" minOccurs="0"/&gt;
 *         &lt;element name="Cntry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MailAddr", propOrder = {
    "st",
    "townCnty",
    "addrZip",
    "cntry"
})
public class MailAddr {

    @XmlElement(name = "St")
    protected St st;
    @XmlElement(name = "TownCnty")
    protected TownCnty townCnty;
    @XmlElement(name = "AddrZip")
    protected AddrZip addrZip;
    @XmlElement(name = "Cntry")
    protected String cntry;

    /**
     * 取得 st 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link St }
     *     
     */
    public St getSt() {
        return st;
    }

    /**
     * 設定 st 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link St }
     *     
     */
    public void setSt(St value) {
        this.st = value;
    }

    /**
     * 取得 townCnty 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link TownCnty }
     *     
     */
    public TownCnty getTownCnty() {
        return townCnty;
    }

    /**
     * 設定 townCnty 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link TownCnty }
     *     
     */
    public void setTownCnty(TownCnty value) {
        this.townCnty = value;
    }

    /**
     * 取得 addrZip 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link AddrZip }
     *     
     */
    public AddrZip getAddrZip() {
        return addrZip;
    }

    /**
     * 設定 addrZip 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link AddrZip }
     *     
     */
    public void setAddrZip(AddrZip value) {
        this.addrZip = value;
    }

    /**
     * 取得 cntry 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCntry() {
        return cntry;
    }

    /**
     * 設定 cntry 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCntry(String value) {
        this.cntry = value;
    }

}
