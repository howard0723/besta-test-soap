
package com.obank.xsd.tw.customer.custprofinqrs.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>AddrZip complex type 的 Java 類別.
 * 
 * <p>下列綱要片段會指定此類別中包含的預期內容.
 * 
 * <pre>
 * &lt;complexType name="AddrZip"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GBAddrZip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TWAddrZip" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AddrZip", propOrder = {
    "gbAddrZip",
    "twAddrZip"
})
public class AddrZip {

    @XmlElement(name = "GBAddrZip", required = true)
    protected String gbAddrZip;
    @XmlElement(name = "TWAddrZip", required = true)
    protected String twAddrZip;

    /**
     * 取得 gbAddrZip 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGBAddrZip() {
        return gbAddrZip;
    }

    /**
     * 設定 gbAddrZip 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGBAddrZip(String value) {
        this.gbAddrZip = value;
    }

    /**
     * 取得 twAddrZip 特性的值.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTWAddrZip() {
        return twAddrZip;
    }

    /**
     * 設定 twAddrZip 特性的值.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTWAddrZip(String value) {
        this.twAddrZip = value;
    }

}
