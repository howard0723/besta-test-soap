package com.obank.wsdl.tw.bankcard.v1;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.1.11.fuse-000243-redhat-1
 * 2019-10-07T10:30:45.392+08:00
 * Generated source version: 3.1.11.fuse-000243-redhat-1
 * 
 */
@WebServiceClient(name = "BankCard", 
                  wsdlLocation = "file:/D:/redhat_fuse/web2/tempSoapDSL/src/main/resources/wsdl/BankCard.wsdl",
                  targetNamespace = "http://www.obank.com/wsdl/TW/BankCard/v1") 
public class BankCard extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://www.obank.com/wsdl/TW/BankCard/v1", "BankCard");
    public final static QName HTTPPortTypeEndpoint = new QName("http://www.obank.com/wsdl/TW/BankCard/v1", "HTTPPortTypeEndpoint");
    static {
        URL url = null;
        try {
            url = new URL("file:/D:/redhat_fuse/web2/tempSoapDSL/src/main/resources/wsdl/BankCard.wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(BankCard.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "file:/D:/redhat_fuse/web2/tempSoapDSL/src/main/resources/wsdl/BankCard.wsdl");
        }
        WSDL_LOCATION = url;
    }

    public BankCard(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public BankCard(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public BankCard() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    public BankCard(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public BankCard(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public BankCard(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }    




    /**
     *
     * @return
     *     returns PortType
     */
    @WebEndpoint(name = "HTTPPortTypeEndpoint")
    public PortType getHTTPPortTypeEndpoint() {
        return super.getPort(HTTPPortTypeEndpoint, PortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PortType
     */
    @WebEndpoint(name = "HTTPPortTypeEndpoint")
    public PortType getHTTPPortTypeEndpoint(WebServiceFeature... features) {
        return super.getPort(HTTPPortTypeEndpoint, PortType.class, features);
    }

}
