package org.mycompany.processer;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.mycompany.impl.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomerRequestProcesser implements Processor {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerRequestProcesser.class);

	@Override
	public void process(Exchange exchange) throws Exception {
		CustomerService cs = new CustomerService();
		cs.createSOAPMessage(new Date(), exchange);
	}

}
