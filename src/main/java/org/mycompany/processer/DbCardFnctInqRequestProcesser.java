package org.mycompany.processer;

import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.mycompany.impl.DbCardFnctInqService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DbCardFnctInqRequestProcesser implements Processor {
	private static final Logger LOGGER = LoggerFactory.getLogger(DbCardFnctInqRequestProcesser.class);

	@Override
	public void process(Exchange exchange) throws Exception {
		DbCardFnctInqService ds = new DbCardFnctInqService();
		ds.createSOAPMessage(new Date(), exchange);
	}

}
