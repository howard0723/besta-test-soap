package org.mycompany.processer;

import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.mycompany.impl.CarddataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CarddataRequestProcesser implements Processor {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerRequestProcesser.class);

	@Override
	public void process(Exchange exchange) throws Exception {
		CarddataService cs = new CarddataService();
		cs.createSOAPMessage(new Date(), exchange);
	}

}
