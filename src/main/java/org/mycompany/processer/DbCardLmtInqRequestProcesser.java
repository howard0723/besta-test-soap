package org.mycompany.processer;

import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.mycompany.impl.CustomerService;
import org.mycompany.impl.DbCardLmtInqService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DbCardLmtInqRequestProcesser implements Processor {
	private static final Logger LOGGER = LoggerFactory.getLogger(DbCardLmtInqRequestProcesser.class);

	@Override
	public void process(Exchange exchange) throws Exception {
		DbCardLmtInqService ds = new DbCardLmtInqService();
		ds.createSOAPMessage(new Date(), exchange);
	}

}
