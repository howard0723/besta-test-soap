package org.mycompany.soap;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.Holder;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.message.MessageContentsList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.iisigroup.bank.xsd.tw.common.soapenv.v1.Header;
import com.obank.xsd.tw.customer.custprofinqrs.v1.CustProfInqRs;


public abstract class AbstractSaopService<T> {
	protected static SimpleDateFormat SDF1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	protected static SimpleDateFormat SDF2 = new SimpleDateFormat("yyyyMMdd");
	protected static String CLIENT_ID = "a1b2c3d4e5f6e7d8c9b0a1";
	private Logger logger = LoggerFactory.getLogger(getClass());
	abstract public void createSOAPMessage(Date date, Exchange exchange) throws Exception;
	abstract protected T createSoapBody(Map<String,String> params);
	
	protected Map<String,Object> getHttpHeaders(){
		Map headerMap = new HashMap();
		headerMap.put("Content-Type", "application/json;charset=UTF-8");
		return headerMap;
	}
	
	protected String getTxid(Date date) {
		String rtn = StringUtils.left(CLIENT_ID, 6);
		rtn = rtn + SDF2.format(date);
		
		try {
			HttpServletRequest request = 
			        ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes())
			                .getRequest();
			rtn = rtn + (StringUtils.isNotBlank(request.getHeader("uuid")) ? request.getHeader("uuid") : "") ;
		} catch (Throwable e) {
			logger.warn(e.getMessage());
		}
		return rtn;
	}
	
	public Holder<Header> getSOAPEnvelopeHeader(String serviceDomain, String operationName ) throws Exception {
		Holder<Header> result = new Holder<Header>();
		Header header = new Header();
		result.value = header;
		Date date = new Date();
		header.setChannelID("ESB");
		header.setServiceDomain(serviceDomain);
		header.setOperationName(operationName);
		header.setConsumerID("TWESB");
		header.setTransactionID(getTxid(date));
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(date);
		XMLGregorianCalendar date2;
		date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		header.setRqTimestamp(date2);
		return result;
	}
	

}
