package org.mycompany.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.camel.CamelContext;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.json.JSONObject;
import org.mycompany.exception.DataNotFoundException;
import org.mycompany.impl.CarddataService;
import org.mycompany.impl.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.obank.xsd.tw.customer.custprofinqrs.v1.CustProfInqRs;

@RestController
public class CustomerCardData {
	@Autowired
	private CamelContext camelContext;
	@Produce(uri = "direct:CustProfInq")
	private ProducerTemplate custProfInqTemplate;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	@GetMapping("/customercarddata")
	public ResponseEntity<Map<String, Object>> carddata(HttpServletRequest request, HttpServletResponse response,
			@RequestHeader(required = false, value = "Content-Type") String contentType,
			@RequestHeader(required = false, value = "authorization") String authorization,
			@RequestHeader(required = false, value = "client_id") String clientID,
			@RequestHeader(required = false, value = "uuid") String uuid,@RequestParam("CIFNo") String cifNo) {
		
		Map<String, Object> rtn = new LinkedHashMap<String,Object>();
		Map<String,Object> responseStatus = new LinkedHashMap<String,Object>();

		String customerId = null;
		try {
			Map<String, String> params = new HashMap<String, String>();
			params.put("customerId", customerId);
			params.put("CIFNo", cifNo);
			Future future = custProfInqTemplate.asyncSendBody(custProfInqTemplate.getDefaultEndpoint(), params);
			CustomerService cs = new CustomerService();
			CustProfInqRs custProfInqRs = cs.getResponse(future);
			if ("E1626".equals(custProfInqRs.getServiceBody().getRtrnStatCode().get(0).getRtrnCode())) {
				throw new DataNotFoundException();
			} else if("00000".equals(custProfInqRs.getServiceBody().getRtrnStatCode().get(0).getRtrnCode()) == false) {
	    		throw new IllegalStateException("unknow response rtn code." + custProfInqRs.getServiceBody().getRtrnStatCode().get(0).getRtrnCode());
	    	}
			
			customerId = custProfInqRs.getServiceBody().getCustProfInfo().getMnem();
			
			JSONObject json = cs.genJSon(custProfInqRs);
			rtn.put("customerData", json.toMap());
		} catch (DataNotFoundException e) {
			logger.error(e.getMessage(),e);
			responseStatus.put("code", "1001");
			responseStatus.put("description", "查無資料");
			rtn.put("responseStatus", responseStatus);
			return new ResponseEntity<Map<String, Object>>(rtn,HttpStatus.NOT_FOUND);
		} catch (Throwable e) {
			logger.error(e.getMessage(),e);
			responseStatus.put("code", "9999");
			responseStatus.put("description", "服務異常");
			rtn.put("responseStatus", responseStatus);
			return new ResponseEntity<Map<String, Object>>(rtn,HttpStatus.INTERNAL_SERVER_ERROR);
		}

		
		try {
			CarddataService cs = new CarddataService();			
			Map<String,Object> card = cs.getCards(camelContext, customerId);
			List<Map<String,Object>> cards = new ArrayList<Map<String,Object>>();
			cards.add(card);
			
			rtn.put("cards", cards);
			return new ResponseEntity<>(rtn, HttpStatus.OK);
		} catch (DataNotFoundException e) {
			rtn.put("cards", new ArrayList<Map<String,Object>>());
			return new ResponseEntity<>(rtn, HttpStatus.OK);
		} catch (Throwable e) {
			logger.error(e.getMessage(),e);
			responseStatus.put("code", "9999");
			responseStatus.put("description", "服務異常");
			rtn.put("responseStatus", responseStatus);
			return new ResponseEntity<Map<String, Object>>(rtn,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
