package org.mycompany.controller;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.camel.CamelContext;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.json.JSONObject;
import org.mycompany.impl.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.obank.xsd.tw.customer.custprofinqrs.v1.CustProfInqRs;

@RestController
@Component
public class ObankController {
	@Autowired
	private CamelContext camelContext;
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Produce(uri = "direct:CustProfInq")
	private ProducerTemplate template;

	@RequestMapping("/customerdata")
	public ResponseEntity<Map<String, Object>> customerdata(HttpServletRequest request, HttpServletResponse response,
			@RequestHeader(required = false, value = "Content-Type") String contentType,
			@RequestHeader(required = false, value = "authorization") String authorization,
			@RequestHeader(required = false, value = "client_id") String clientID,
			@RequestHeader(required = false, value = "uuid") String uuid,
			@RequestParam(required = false, value = "customerId") String customerId,
			@RequestParam(required = false, value = "CIFNo") String CIFNo) {
		JSONObject json = null;
		Map<String, Object> rtn = new LinkedHashMap<String,Object>();
    	Map<String,Object> responseStatus = new LinkedHashMap<String,Object>();
		Map<String, String> params = new HashMap<String, String>();
		params.put("customerId", customerId);
		params.put("CIFNo", CIFNo);

		try {
			Future future = template.asyncSendBody(template.getDefaultEndpoint(), params);
			CustomerService cs = new CustomerService();
			CustProfInqRs custProfInqRs = cs.getResponse(future);
			if(custProfInqRs != null) {
				if("00000".equals(custProfInqRs.getServiceBody().getRtrnStatCode().get(0).getRtrnCode())) {
		    		json = cs.genJSon(custProfInqRs);
		    		return new ResponseEntity<Map<String, Object>>(json.toMap(),HttpStatus.OK);		    		
		    	}else if ("E1626".equals(custProfInqRs.getServiceBody().getRtrnStatCode().get(0).getRtrnCode())) {
		    		logger.error("查無資料");
		    		responseStatus.put("code", "1001");
					responseStatus.put("description", "查無資料");
					rtn.put("responseStatus", responseStatus);
					return new ResponseEntity<Map<String, Object>>(rtn,HttpStatus.NOT_FOUND);		    		
				} else {
					logger.error("unknow response rtn code.");
					responseStatus.put("code", "9999");
					responseStatus.put("description", "服務異常");
					rtn.put("responseStatus", responseStatus);
					return new ResponseEntity<Map<String, Object>>(rtn,HttpStatus.INTERNAL_SERVER_ERROR);
				}		
			}else {
				responseStatus.put("code", "9999");
				responseStatus.put("description", "服務異常");
				rtn.put("responseStatus", responseStatus);
				return new ResponseEntity<Map<String, Object>>(rtn,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}catch (Throwable e) {
			logger.error(e.getMessage(),e);
			responseStatus.put("code", "9999");
			responseStatus.put("description", "服務異常");
			rtn.put("responseStatus", responseStatus);
			return new ResponseEntity<Map<String, Object>>(rtn,HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			logger.info("get customer controller end........");
		}
	}

}
