package org.mycompany.impl;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.xml.ws.Holder;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.cxf.message.MessageContentsList;
import org.json.JSONObject;
import org.mycompany.soap.AbstractSaopService;

import com.iisigroup.bank.xsd.tw.common.soapenv.v1.Header;
import com.obank.xsd.tw.customer.custprofinqrq.v1.CustProfInqRq;
import com.obank.xsd.tw.customer.custprofinqrq.v1.ServiceBody;
import com.obank.xsd.tw.customer.custprofinqrq.v1.ServiceHeaderType;
import com.obank.xsd.tw.customer.custprofinqrq.v1.SignonType;
import com.obank.xsd.tw.customer.custprofinqrs.v1.CustProfInfo;
import com.obank.xsd.tw.customer.custprofinqrs.v1.CustProfInqRs;

public class CustomerService extends AbstractSaopService<CustProfInqRq>{

	public CustProfInqRs getResponse(Future future) throws InterruptedException, ExecutionException {
		if(future != null && future.get() != null) {
			MessageContentsList list = (MessageContentsList) future.get();
			for(Object o :list) {
				if(o instanceof CustProfInqRs) {
					return (CustProfInqRs) o;
				}
			}
		}
		return null;
	}
	
	@Override
	public void createSOAPMessage(Date date, Exchange exchange) throws Exception {
		Object[] args = exchange.getIn().getBody(Object[].class);
		Map<String,String> params = null;
		if(args != null) {
			params = (Map<String, String>) args[0];
		}	
		
		Message out = exchange.getOut();
		Map<String, Object> httpHeaders = this.getHttpHeaders();
		out.setHeaders(httpHeaders);		
		
		Holder<Header> soapHeader = this.getSOAPEnvelopeHeader("Customer", "CustProfInq");
		CustProfInqRq custProfInqRq = createSoapBody(params);
		out.setBody(new Object[] {soapHeader, custProfInqRq});
	}

	@Override
	protected CustProfInqRq createSoapBody(Map<String, String> params) {
		CustProfInqRq custProfInqRq = new CustProfInqRq();
		// set ServiceHeaderType
		ServiceHeaderType header = new ServiceHeaderType();		
		header.setTxnNo("T0982323");
		header.setTxnId("XX001");
		
		// set ServiceBody
		ServiceBody body = new ServiceBody();
		if(params.get("customerId") != null && params.get("customerId").length() > 0) {
			body.setCustPermId(params.get("customerId"));
		}else if (params.get("CIFNo") != null  && params.get("CIFNo").length() > 0) {
			body.setCIFNo(params.get("CIFNo"));
		}
		
		// set SignonType
		SignonType singon = new SignonType();
		singon.setCustId("IFUTBSF0001");
		singon.setAuthToken("0");
		
		custProfInqRq.setServiceBody(body);
		custProfInqRq.setServiceHeader(header);
		custProfInqRq.setSignon(singon);
		return custProfInqRq;
	}

	public JSONObject genJSon(CustProfInqRs custProfInqRs) {
		JSONObject json = new JSONObject();
		CustProfInfo custProfInfo = custProfInqRs.getServiceBody().getCustProfInfo();
		json.put("CIFNo", custProfInfo.getCIFNo());
		json.put("customerId", custProfInfo.getMnem());
		json.put("chineseName", custProfInfo.getCustChnName());
		json.put("nickName", custProfInfo.getCustShrtName());
		json.put("birthday", custProfInfo.getBirthDay());
		json.put("cellphoneNo", custProfInfo.getMobNo1());
		json.put("email", custProfInfo.getEMailAddr1());
		JSONObject addressNodeJson = new JSONObject();
		JSONObject streetNodeJson = new JSONObject();
		json.put("address", addressNodeJson);
		addressNodeJson.put("street", streetNodeJson);
		streetNodeJson.put("global", custProfInfo.getMailAddr().getSt().getGBSt());
		streetNodeJson.put("taiwan", custProfInfo.getMailAddr().getSt().getTWSt());
		
		JSONObject townNodeJson = new JSONObject();
		townNodeJson.put("global", custProfInfo.getMailAddr().getTownCnty().getGBTownCnty());
		townNodeJson.put("taiwan", custProfInfo.getMailAddr().getTownCnty().getTWTownCnty());
		
		json.put("townCountry",townNodeJson);
		
		JSONObject zipNodeJson = new JSONObject();
		zipNodeJson.put("global", custProfInfo.getMailAddr().getAddrZip().getGBAddrZip());
		zipNodeJson.put("taiwan", custProfInfo.getMailAddr().getAddrZip().getTWAddrZip());
		
		json.put("zip", zipNodeJson);
		json.put("country", custProfInfo.getMailAddr().getCntry());
//		System.out.println("json::::" + json);
		return json;
	}

}
