package org.mycompany.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.xml.ws.Holder;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.cxf.message.MessageContentsList;
import org.mycompany.exception.DataNotFoundException;
import org.mycompany.soap.AbstractSaopService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.iisigroup.bank.xsd.tw.common.soapenv.v1.Header;
import com.obank.xsd.tw.bankcard.bankcardsumminqrq.v1.BankCardSummInqRq;
import com.obank.xsd.tw.bankcard.bankcardsumminqrq.v1.ServiceBody;
import com.obank.xsd.tw.bankcard.bankcardsumminqrq.v1.ServiceHeaderType;
import com.obank.xsd.tw.bankcard.bankcardsumminqrq.v1.SignonType;
import com.obank.xsd.tw.bankcard.bankcardsumminqrs.v1.BankCardRec;
import com.obank.xsd.tw.bankcard.bankcardsumminqrs.v1.BankCardSummInqRs;
import com.obank.xsd.tw.debitcard.dbcardfnctinqrs.v1.CardBlkRec;
import com.obank.xsd.tw.debitcard.dbcardfnctinqrs.v1.DbCardFnctInqRs;
import com.obank.xsd.tw.debitcard.dbcardlmtinqrs.v1.CardLmtRec;
import com.obank.xsd.tw.debitcard.dbcardlmtinqrs.v1.DbCardLmtInqRs;

@Component
public class CarddataService extends AbstractSaopService<BankCardSummInqRq>{

	@Produce(uri = "direct:BankCardSummInq")
	private ProducerTemplate template;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	
	public Map<String,Object> invokeMultiRoutes(CamelContext camelContext , String cardNO) throws Throwable {
		ProducerTemplate producerTemplate = camelContext.createProducerTemplate();
		Map<String, String> params = new HashMap<String, String>();
		params.put("cardNO", cardNO);
		Future future = producerTemplate.asyncSendBody("direct:MultiDbCard", params);
		logger.info("multi response ............." + future.get());
		Map<String,Object> response = new HashMap<String,Object>();
		if(future != null) {
			ArrayList messageList = (ArrayList) future.get();
//			logger.info("messageList ............." + messageList);
			for(Object list : messageList) {
//				logger.info("object list ............." + list + ",instanceof ArrayList::" + (list instanceof ArrayList));
				if(list instanceof ArrayList) {
					List tempList = (ArrayList)list;
					for(Object o : tempList) {
//						logger.info("object  ............." + o);
						if(o instanceof DbCardLmtInqRs) {
//							logger.info("o instanceof DbCardLmtInqRs  .............");
							response.put("DbCardLmtInqRs", (DbCardLmtInqRs) o) ;
						}else if (o instanceof DbCardFnctInqRs) {
//							logger.info("o instanceof DbCardFnctInqRs  .............");
							response.put("DbCardFnctInqRs", (DbCardFnctInqRs) o) ;
						}
					}
				}
			}
		}
		return response;
	}
	
	
	public Map<String,Object> getCards(CamelContext camelContext, String customerId)throws Throwable {
		// init template
		ProducerTemplate producerTemplate = camelContext.createProducerTemplate();
		this.template = producerTemplate;
		
		BankCardSummInqRs summ = this.getCartNo(customerId);
		com.obank.xsd.tw.bankcard.bankcardsumminqrs.v1.ServiceBody sb = summ.getServiceBody();
		BankCardRec bcr = sb.getBankCardRec().get(0);
		String cardNo = bcr.getCardNo();
		logger.info("cardNo:::::::::::::::" + cardNo);
		
		Map<String,Object> response = invokeMultiRoutes(camelContext, cardNo);
		logger.info("response:::::::::::::::" + response);
		DbCardLmtInqRs dblr = (DbCardLmtInqRs) response.get("DbCardLmtInqRs");
		DbCardFnctInqRs dbfr = (DbCardFnctInqRs) response.get("DbCardFnctInqRs");
//		DbCardLmtInqRs dblr = this.getDebitCardImtIng(cardNo);
//		DbCardFnctInqRs dbfr = this.getDebitCardFnctIng(cardNo);

		if (!("92".equals(dblr.getServiceBody().getRtrnCode()) || "00".equals(dblr.getServiceBody().getRtrnCode()))) {
			throw new IllegalStateException(dblr.getServiceBody().getRtrnCode());
		}
		if (!("91".equals(dbfr.getServiceBody().getRtrnCode()) || "00".equals(dbfr.getServiceBody().getRtrnCode()))) {
			throw new IllegalStateException(dbfr.getServiceBody().getRtrnCode());
		}
		
		CardLmtRec clr = null;
		if (CollectionUtils.isEmpty(dblr.getServiceBody().getCardLmtRec()) == false) {
			clr = dblr.getServiceBody().getCardLmtRec().get(0);
		}
		
		CardBlkRec cbr = null;
		if (CollectionUtils.isEmpty(dbfr.getServiceBody().getCardBlkRec()) == false) {
			cbr = dbfr.getServiceBody().getCardBlkRec().get(0);
		}
		
		Map<String,Object> cards = new LinkedHashMap<String,Object>();
		
		cards.put("cardNo", cardNo);
		cards.put("cardType", bcr.getCardFaceName());
		logger.info("cardType:::::::::::::::" + bcr.getCardFaceName());
		cards.put("cardStatus", bcr.getCardIssueStat());
		
		if (clr != null) {
			cards.put("dailyLimitAmount", clr.getAvailDayLmt());
			cards.put("monthlyLimitAmount", clr.getAvailMnthLmt());
			cards.put("usedDailyAmount", clr.getAccumDayLmt());
			cards.put("usedMonthlyAmount", clr.getAccumMnthLmt());
		} else {
			cards.put("dailyLimitAmount", "");
			cards.put("monthlyLimitAmount", "");
			cards.put("usedDailyAmount", "");
			cards.put("usedMonthlyAmount", "");
		}
		
		if (cbr != null) {
			cards.put("expireMonthYear", cbr.getExpYearMnth());
		} else {
			cards.put("expireMonthYear", "");
		}
		return cards;
	}
	
	private DbCardLmtInqRs getDebitCardImtIng(String cardNO) throws Throwable{
		DbCardLmtInqRs result = new DbCardLmtInqRs();
		Map<String, String> params = new HashMap<String, String>();
		params.put("cardNO", cardNO);
		Future future = this.template.asyncSendBody("direct:DbCardLmtInq", params);
		DbCardLmtInqRs dblr = new DbCardLmtInqService().getResponse(future);
		logger.info("DbCardLmtInqRs:::::::::::::::::" + dblr);
		if (!("92".equals(dblr.getServiceBody().getRtrnCode()) || "00".equals(dblr.getServiceBody().getRtrnCode()))) {
			throw new IllegalStateException(dblr.getServiceBody().getRtrnCode());
		}
		return dblr;
	}
	
	private DbCardFnctInqRs getDebitCardFnctIng(String cardNO) throws Throwable {
		Map<String, String> params = new HashMap<String, String>();
		params.put("cardNO", cardNO);
		Future future = this.template.asyncSendBody("direct:DbCardFnctInq", params);
		DbCardFnctInqRs dbfr = new DbCardFnctInqService().getResponse(future);
		logger.info("DbCardFnctInqRs:::::::::::::::::" + dbfr);
		if (!("91".equals(dbfr.getServiceBody().getRtrnCode()) || "00".equals(dbfr.getServiceBody().getRtrnCode()))) {
			throw new IllegalStateException(dbfr.getServiceBody().getRtrnCode());
		}
		return dbfr;
	}
	
	private BankCardSummInqRs getCartNo(String customerId)throws Throwable {
		Map<String, String> params = new HashMap<String, String>();
		params.put("customerId", customerId);
		logger.info("template:::::::::::::::" + this.template);
		Future future = this.template.asyncSendBody("direct:BankCardSummInq", params);
		
		BankCardSummInqRs bankCardSummInqRs = this.getResponse(future);
		logger.info("bankCardSummInqRs:::::::::::::::::" + bankCardSummInqRs);
		com.obank.xsd.tw.bankcard.bankcardsumminqrs.v1.ServiceBody sb = bankCardSummInqRs.getServiceBody();
		if ("98".equals(sb.getRtrnCode())) {
			throw new DataNotFoundException();
		} else if ("00".equals(sb.getRtrnCode()) == false) {
			throw new IllegalStateException(sb.getRtrnCode());
		}
		return bankCardSummInqRs;
	}
	
	
	public BankCardSummInqRs getResponse(Future future) throws InterruptedException, ExecutionException {
		if(future != null && future.get() != null) {
			MessageContentsList list = (MessageContentsList) future.get();
			for(Object o :list) {
				if(o instanceof BankCardSummInqRs) {
					return (BankCardSummInqRs) o;
				}
			}
		}
		return null;
	}
	
	@Override
	public void createSOAPMessage(Date date, Exchange exchange) throws Exception {
		Object[] args = exchange.getIn().getBody(Object[].class);
		Map<String,String> params = null;
		if(args != null) {
			params = (Map<String, String>) args[0];
		}	
		
		Message out = exchange.getOut();
		Map<String, Object> httpHeaders = this.getHttpHeaders();
		out.setHeaders(httpHeaders);		
		
		Holder<Header> soapHeader = this.getSOAPEnvelopeHeader("BankCard", "BankCardSummInq");
		BankCardSummInqRq bankCardSummInqRq = createSoapBody(params);
		out.setBody(new Object[] {soapHeader, bankCardSummInqRq});
	}

	@Override
	protected BankCardSummInqRq createSoapBody(Map<String, String> params) {
		BankCardSummInqRq bankCardSummInqRq = new BankCardSummInqRq();
		// set ServiceHeaderType
		ServiceHeaderType header = new ServiceHeaderType();		
		header.setTxnNo("T0982323");
		header.setTxnId("XX001");
		
		// set ServiceBody
		com.obank.xsd.tw.bankcard.bankcardsumminqrq.v1.ServiceBody body = new ServiceBody();
		if(params.get("customerId") != null && params.get("customerId").length() > 0) {
			body.setInqKeyId(params.get("customerId"));
		}
		body.setPageNo(BigInteger.valueOf(1));
		body.setNoOfPage(BigInteger.valueOf(200));
		body.setInqType("2");
		
		// set SignonType
		SignonType singon = new SignonType();
		singon.setCustId("IFUTBSF0001");
		singon.setAuthToken("0");
		
		bankCardSummInqRq.setServiceBody(body);
		bankCardSummInqRq.setServiceHeader(header);
		bankCardSummInqRq.setSignon(singon);
		return bankCardSummInqRq;
	}
}
