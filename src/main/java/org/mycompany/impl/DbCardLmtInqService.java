package org.mycompany.impl;

import java.math.BigInteger;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.xml.ws.Holder;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.cxf.message.MessageContentsList;
import org.mycompany.soap.AbstractSaopService;

import com.iisigroup.bank.xsd.tw.common.soapenv.v1.Header;
import com.obank.xsd.tw.debitcard.dbcardlmtinqrq.v1.DbCardLmtInqRq;
import com.obank.xsd.tw.debitcard.dbcardlmtinqrq.v1.ServiceBody;
import com.obank.xsd.tw.debitcard.dbcardlmtinqrq.v1.ServiceHeaderType;
import com.obank.xsd.tw.debitcard.dbcardlmtinqrq.v1.SignonType;
import com.obank.xsd.tw.debitcard.dbcardlmtinqrs.v1.DbCardLmtInqRs;

public class DbCardLmtInqService extends AbstractSaopService<DbCardLmtInqRq>{

	public DbCardLmtInqRs getResponse(Future future) throws InterruptedException, ExecutionException {
		if(future != null && future.get() != null) {
			MessageContentsList list = (MessageContentsList) future.get();
			for(Object o :list) {
				if(o instanceof DbCardLmtInqRs) {
					return (DbCardLmtInqRs) o;
				}
			}
		}
		return null;
	}
	
	@Override
	public void createSOAPMessage(Date date, Exchange exchange) throws Exception {
		Object[] args = exchange.getIn().getBody(Object[].class);
		Map<String,String> params = null;
		if(args != null) {
			params = (Map<String, String>) args[0];
		}
		Message out = exchange.getOut();
		Map<String, Object> httpHeaders = this.getHttpHeaders();
		out.setHeaders(httpHeaders);		
		
		Holder<Header> soapHeader = this.getSOAPEnvelopeHeader("DebitCard", "DbCardLmtInq");
		DbCardLmtInqRq dbCardLmtInqRq = createSoapBody(params);
		out.setBody(new Object[] {soapHeader, dbCardLmtInqRq});
	}

	@Override
	protected DbCardLmtInqRq createSoapBody(Map<String, String> params) {
		DbCardLmtInqRq dbCardLmtInqRq = new DbCardLmtInqRq();
		// set ServiceHeaderType
		ServiceHeaderType header = new ServiceHeaderType();
		header.setTxnNo("T0982323");
		header.setTxnId("XX001");

		// set ServiceBody
		ServiceBody body = new ServiceBody();
		if (params.get("cardNO") != null && params.get("cardNO").length() > 0) {
			body.setInqKeyId(params.get("cardNO"));
		} 
		body.setPageNo(BigInteger.valueOf(1));
		body.setNoOfPage(BigInteger.valueOf(200));
		body.setInqType("2");
		
		// set SignonType
		SignonType singon = new SignonType();
		singon.setCustId("IFUTBSF0001");
		singon.setAuthToken("0");
		dbCardLmtInqRq.setServiceBody(body);
		dbCardLmtInqRq.setServiceHeader(header);
		dbCardLmtInqRq.setSignon(singon);
		return dbCardLmtInqRq;
	}

}
